import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ChannelService} from '../../service/channel.service';
import {ActivatedRoute} from '@angular/router';
import {ServerResult} from '../../model/ServerResult';
import {Channel} from '../../model/Channel';

@Component({
  selector: 'app-channel',
  templateUrl: './channel.component.html',
  styleUrls: ['./channel.component.css']
})
export class ChannelComponent implements OnInit {
  public ListChannel: Array<Channel>;

  constructor(private http: HttpClient, private route: ActivatedRoute, private channelService: ChannelService) {
  }

  ngOnInit(): void {
    this.setListChannel();
  }

  setListChannel() {
    this.route.params.subscribe((params) => {
      const serverId = +params.serverId;
      this.channelService.listChannel(serverId).subscribe((data: ServerResult) => {
        this.ListChannel = data.channel;
      });
    });
  }
}
