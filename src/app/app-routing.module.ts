import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {HomeComponent} from './home/home.component';
import {ChannelComponent} from './AddToolHome/channel/channel.component';
import {FriendsComponent} from './home/Friends/friends.component';
import {MessagesComponent} from './messages/messages.component';


const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'login'},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {
    path: 'homepage', component: HomeComponent, children:
      [
        {
          path: '@me', children:
            [
              {path: '', component: FriendsComponent, outlet: 'list'},
              {path: ':id', component: MessagesComponent},
            ]
        },
        {
          path: 'channel', children:
            [
              {
                path: ':serverId', children:
                  [
                    {path: '', component: ChannelComponent, outlet: 'list'},
                    {path: ':id', component: MessagesComponent},
                  ]
              },
            ]
        },
      ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
