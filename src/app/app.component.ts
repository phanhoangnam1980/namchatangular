import {Component} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from './model/User';
import {Subscription} from 'rxjs';
import {AuthenticationService} from './service/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public currentUser: User;
  private currentUserSubsciption: Subscription;
  title = 'NamChatAngular';
  name: string;

  constructor(private http: HttpClient, private authenticationService: AuthenticationService) {
    this.currentUserSubsciption = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
    });
  }

}
