import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {UserComponent} from './user/user.component';
import {MessagesComponent} from './messages/messages.component';
import {FriendsComponent} from './home/Friends/friends.component';
import {RegisterComponent} from './register/register.component';
import {LoginComponent} from './login/login.component';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatListModule} from '@angular/material/list';
import {MatInputModule} from '@angular/material/input';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {HomeComponent} from './home/home.component';
import {MatDialogModule} from '@angular/material/dialog';
import {SetServerComponent} from './modal/SetServer.component';
import {FindServerComponent} from './modal/FindServer.component';
import {LoginService} from './service/login.service';
import {AuthenticationService} from './service/authentication.service';
import {RegisterService} from './service/register.service';
import {SearchResultComponent} from './home/search-result/search-result.component';
import {UserchatComponent} from './messages/userchat/userchat.component';
import {ChannelChatComponent} from './messages/channel-chat/channel-chat.component';
import {SocketService} from './service/socket.service';
import {MessagesService} from './service/messages.service';
import {ServerService} from './service/server.service';
import {ListFriendComponent} from './list-friend/list-friend.component';
import {ChannelService} from './service/channel.service';
import {SearchService} from './service/search.service';
import {SwitchDataHomeService} from './service/SwitchDataHome.service';
import {ImageMessageDirective} from './messages/ImageMessage.directive';
import {ChannelComponent} from './AddToolHome/channel/channel.component';


@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    MessagesComponent,
    FriendsComponent,
    RegisterComponent,
    LoginComponent,
    HomeComponent,
    SetServerComponent,
    FindServerComponent,
    ChannelComponent,
    SearchResultComponent,
    UserchatComponent,
    ChannelChatComponent,
    ListFriendComponent,
    ImageMessageDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatListModule,
    MatInputModule,
    MatToolbarModule,
    MatSidenavModule,
    MatDialogModule
  ],
  providers: [LoginService, AuthenticationService, RegisterService, SocketService,
    MessagesService, UserchatComponent, ServerService, ChannelService, SearchResultComponent, SearchService, SwitchDataHomeService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
