import {
  AfterViewChecked,
  AfterViewInit,
  Component,
  Injectable,
  OnInit
} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MessagesResult} from '../../model/MessagesResult';
import {FriendService} from '../../service/Friend.service';
import {SwitchDataHomeService} from '../../service/SwitchDataHome.service';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css']
})
@Injectable({
  providedIn: 'root',
})
export class FriendsComponent implements OnInit, AfterViewInit, AfterViewChecked {
  public dataFriend: MessagesResult[];
  checkRequestFriend = false;
  public FriendInfo: MessagesResult;

  constructor(private http: HttpClient, private friendService: FriendService, private shareData: SwitchDataHomeService) {
    this.ListFriends();
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.shareData.currentCheckFriend.subscribe(check => {
      this.checkRequestFriend = check;
    });
    this.shareData.currentFriendInfo.subscribe(Info => {
      this.FriendInfo = Info;
    });
  }

  ngAfterViewChecked() {
    if (this.checkRequestFriend && this.FriendInfo) {
      this.dataFriend.push(this.FriendInfo);
    }
  }

  ListFriends() {
    this.friendService.listFriend().subscribe((data: MessagesResult[]) => {
      this.dataFriend = data;
    });
  }
}
