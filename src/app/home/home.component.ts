import {Component, OnInit, AfterViewInit, Input} from '@angular/core';
import {User} from '../model/User';
import {Subscription} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {AuthenticationService} from '../service/authentication.service';
import {MatDialog} from '@angular/material/dialog';
import {SetServerComponent} from '../modal/SetServer.component';
import {FindServerComponent} from '../modal/FindServer.component';
import {Router} from '@angular/router';
import {ServerResult} from '../model/ServerResult';
import {ServerService} from '../service/server.service';
import {Server} from '../model/Server';
import {SearchResultComponent} from './search-result/search-result.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewInit {
  public currentUser: User;
  @Input()
  dataSearchFriend: string;
  searchData = false;
  private currentUserSubsciption: Subscription;
  public Server: Server[];

  constructor(private _HTTP: HttpClient, private authenticationService: AuthenticationService, public dialog: MatDialog,
              private router: Router, private serverService: ServerService, private searchResult: SearchResultComponent) {
    this.currentUserSubsciption = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
      if (!user) {
        this.currentUser = JSON.parse(localStorage.getItem('token'));
        if (!this.currentUser) {
          this.router.navigate(['login']).then();
        }
      }
    });
    this.serverService.listServer().subscribe((listServerData: ServerResult) => {
      this.Server = listServerData.servers;
    });
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.router.events.subscribe(() => {
      this.searchData = false;
    });
  }

  SearchResult(dataSearchFriend) {
    if (this.router.url === '/homepage/@me') {
      this.searchResult.getListNewFriend(dataSearchFriend);
      this.searchData = !this.searchData;
    }
  }

  openSetServer() {
    this.dialog.open(SetServerComponent, {
      width: '600px',
      data: {name: ''}
    });
  }

  openFindServer() {
    this.dialog.open(FindServerComponent, {
      width: '650px',
      data: {name: ''}
    });
  }

  logout(): any {
    this.currentUser = null;
    window.localStorage.clear();
    this.router.navigate(['login']).then();
  }

}

