import {AfterViewInit, Component, DoCheck, Input, OnInit} from '@angular/core';
import {SearchService} from '../../service/search.service';
import {User} from '../../model/User';
import {MessagesResponse} from '../../model/MessagesResponse';
import {FriendService} from '../../service/Friend.service';
import {MessagesService} from '../../service/messages.service';
import {FriendsComponent} from '../Friends/friends.component';
import {MessagesResult} from '../../model/MessagesResult';
import {Observable} from 'rxjs';
import {SwitchDataHomeService} from '../../service/SwitchDataHome.service';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css']
})
export class SearchResultComponent implements OnInit, AfterViewInit {
  public dataFriend: User[];
  public dataId: number;
  @Input() friendName: string;

  constructor(private searchService: SearchService, private friendService: FriendService,
              private messagesService: MessagesService, private shareData: SwitchDataHomeService) {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.getListNewFriend(this.friendName);
  }

  getListNewFriend(data: string) {
    if (this.friendName) {
      this.searchService.SearchResult(data).subscribe((NewDataFriend: MessagesResponse) => {
        this.dataFriend = NewDataFriend.result;
      });
    }
  }

  SendGetFriend(event) {
    this.dataId = event.target.getAttribute('dataId');
    this.friendService.getFriendId(this.dataId).subscribe(() => {
    });
    this.messagesService.setUpDMGroup(this.dataId).subscribe((newFriendData: MessagesResult) => {
      this.shareData.setFriendInfo(newFriendData);
      this.shareData.setCheckRequest(true);
    });
  }
}
