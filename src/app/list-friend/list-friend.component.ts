import {Component, Injectable, OnInit} from '@angular/core';

@Component({
  selector: 'app-list-friend',
  templateUrl: './list-friend.component.html',
  styleUrls: ['./list-friend.component.css']
})
@Injectable({
  providedIn: 'root',
})
export class ListFriendComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }
}
