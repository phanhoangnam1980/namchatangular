import {LoginService} from '../service/login.service';
import {User} from '../model/User';
import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @Input()
  users: User;
  public messages: string;
  public error: string;

  constructor(private loginService: LoginService, private router: Router) {
    this.users = new User();
  }

  ngOnInit(): void {
  }

  login() {
    this.loginService.login(this.users).subscribe(isValid => {
      if (isValid) {
        localStorage.setItem(
          'token', JSON.stringify({email: this.users.email})
        );
        localStorage.setItem('authentication', JSON.stringify(isValid));
        this.router.navigate(['homepage/@me']).then();
      } else {
        this.error = 'Your email and password is invalid.';
      }
    });
  }
}
