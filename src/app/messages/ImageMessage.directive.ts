import {Directive, EventEmitter, HostBinding, HostListener, Output} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {ImageMessageForm} from '../model/ImageMessageForm';
import {ActivatedRoute} from '@angular/router';

@Directive({
  selector: '[appImageInside]'
})
export class ImageMessageDirective {
  public fileImage: ImageMessageForm[] = [];
  @HostBinding('class.ImageOver') fileOver: boolean;
  @Output() sessionImage = new EventEmitter<ImageMessageForm[]>();
  @Output() Image = new EventEmitter<FormData>();

  setImage(evt: DragEvent) {
    const files = evt.dataTransfer.files[0];
    const url = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(files));
    const newImageFile = new ImageMessageForm();
    newImageFile.channelId = +this.route.snapshot.paramMap.get('id');
    newImageFile.image = files;
    newImageFile.url = url;
    this.fileImage.push(newImageFile);
    if (this.fileImage.length > 0) {
      this.sessionImage.emit(this.fileImage);
    }
  }

  constructor(private sanitizer: DomSanitizer, private route: ActivatedRoute) {
  }

  @HostListener('dragover', ['$event'])
  public onDragOver(evt) {
    evt.preventDefault();
    evt.stopPropagation();
  }

  @HostListener('dragleave', ['$event'])
  public onDragLeave(evt) {
    evt.preventDefault();
    evt.stopPropagation();
  }

  @HostListener('drop', ['$event'])
  public onDrop(evt: DragEvent) {
    evt.preventDefault();
    evt.stopPropagation();
    this.setImage(evt);
  }
}
