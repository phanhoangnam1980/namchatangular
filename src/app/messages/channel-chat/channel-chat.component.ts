import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {MessagesResponse} from '../../model/MessagesResponse';
import {ChannelService} from '../../service/channel.service';
import {ActivatedRoute} from '@angular/router';
import {MessagesResult} from '../../model/MessagesResult';
import {Messages} from '../../model/messages';
import {MessagesService} from '../../service/messages.service';
import {SocketService} from '../../service/socket.service';

@Component({
  selector: 'app-channel-chat',
  templateUrl: './channel-chat.component.html',
  styleUrls: ['./channel-chat.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ChannelChatComponent implements OnInit {
  ListMessages: Messages[];
  private MessageData: Messages;
  private friendDetail: MessagesResult;

  constructor(private channelService: ChannelService, private route: ActivatedRoute,
              private messagesService: MessagesService, private socketService: SocketService) {
  }

  ngOnInit(): void {
    this.mappingChannel();
    this.route.params.subscribe((params) => {
      const userId = +params.id;
      this.messagesService.getDetailId(userId).subscribe((data: MessagesResult) => {
        console.log(data);
        this.friendDetail = data;
      });
      this.ListMessage().subscribe((data: MessagesResponse) => {
        console.log(data);
        this.ListMessages = data.messagesresult;
      });
    });
    this.socketService.startChat();
  }

  mappingChannel() {
    this.route.params.subscribe((params) => {
      const channelId = +params.id;
      this.channelService.mappingChanel(channelId).subscribe((data: MessagesResponse) => {
        console.log(data);
      });
    });
  }

  MessageChat() {
    // this.socketService.sendMessagesSockJS(this.MessageData);
    this.socketService.sendMessage(this.MessageData);
  }

  ListMessage() {
    return this.messagesService.listMessages(+this.route.snapshot.paramMap.get('id'));
  }
}
