import {
  AfterViewChecked,
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnInit,
  ViewChild
} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Messages} from '../model/messages';
import {MessagesResult} from '../model/MessagesResult';
import {SocketService} from '../service/socket.service';
import {ActivatedRoute} from '@angular/router';
import {MessagesService} from '../service/messages.service';
import {MessagesResponse} from '../model/MessagesResponse';
import {User} from '../model/User';
import {ChannelService} from '../service/channel.service';
import {ImageMessageForm} from '../model/ImageMessageForm';
import {DomSanitizer} from '@angular/platform-browser';
import {ImageService} from '../service/image.service';

@Component({
  selector: 'app-message-list',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit, AfterViewChecked, AfterViewInit {

  @ViewChild('scrollBottom') private myScrollContainer: ElementRef;
  ListMessages: Messages[] = [];
  @Input()
  dataMessage: string;
  public scrollTop;
  public preViewImage: ImageMessageForm[] = [];
  private UserChat: User;
  private UserID: number;
  private readonly MessageData: Messages;
  private friendDetail: MessagesResult;
  private listNextMessage: Array<Messages>;
  private typing = false;

  constructor(public socketService: SocketService, private route: ActivatedRoute, private http: HttpClient,
              private messagesService: MessagesService, public imageService: ImageService,
              private cdRef: ChangeDetectorRef, private channelService: ChannelService) {
    this.MessageData = new Messages();
  }

  ngOnInit(): void {
    this.LayoutMessage();
  }

  ngAfterViewInit(): void {
  }

  ngAfterViewChecked(): void {
    this.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    if (this.scrollTop) {
      this.cdRef.detectChanges();
    }
  }

  MessageChat() {
    this.MessageData.session = this.dataMessage;
    this.MessageData.groupid = this.UserID;
    this.UserChat = JSON.parse(localStorage.getItem('token'));
    this.MessageData.userchats = this.UserChat.email;
    this.socketService.sendMessage(this.MessageData);
    this.dataMessage = '';
  }

  Typing() {
    this.typing = true;
    this.UserChat = JSON.parse(localStorage.getItem('token'));
    this.MessageData.userchats = this.UserChat.email;
    this.socketService.sendTyping(this.MessageData.userchats, this.typing);
    console.log('typing');
  }

  StopTyping() {
    console.log('Timeout');
  }

  ListMessage() {
    return this.messagesService.listMessages(+this.route.snapshot.paramMap.get('id'));
  }

  ListNextMessage() {
    this.messagesService.nextMessages(+this.route.snapshot.paramMap.get('id')).subscribe((data: MessagesResponse) => {
      this.listNextMessage = data.messagesresult;
      console.log(data);
      this.ListMessages.unshift(...this.listNextMessage);
    });
  }

  LayoutMessage() {
    this.mappingChannel();
    this.route.params.subscribe((params) => {
      this.UserID = +params.id;
      this.messagesService.getDetailId(this.UserID).subscribe((data: MessagesResult) => {
        this.friendDetail = data;
      });
      this.ListMessage().subscribe((data: MessagesResponse) => {
        this.ListMessages = data.messagesresult;
      });
    });
    this.socketService.startChat();
  }

  ScrollTop(event) {
    if (event.currentTarget.scrollTop === 0) {
      this.ListNextMessage();
      console.log(event.currentTarget.scrollTop);
    }
  }

  mappingChannel() {
    this.route.params.subscribe((params) => {
      const channelId = +params.id;
      this.channelService.mappingChanel(channelId).subscribe(() => {
      });
    });
  }

  addImageUpload(event: ImageMessageForm[]) {
    this.preViewImage = event;
    console.log(event);
    if (this.preViewImage) {
      for (const data of this.preViewImage) {
        this.messagesService.sendImageMessages(data);
      }
      this.preViewImage = [];
    }
  }

  clickFileImage(fileImage: HTMLInputElement) {
    console.log(fileImage.files[0]);
    console.log(+this.route.snapshot.paramMap.get('id'));
    this.preViewImage.push(this.imageService.ClickFile(fileImage.files[0], +this.route.snapshot.paramMap.get('id')));
    if (fileImage.value.length !== 0) {
      for (const data of this.preViewImage) {
        this.messagesService.sendImageMessages(data);
      }
      this.preViewImage = [];
    }
  }
}
