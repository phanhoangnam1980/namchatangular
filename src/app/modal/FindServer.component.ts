import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UserServer} from '../model/userServer';
import {HttpClient} from '@angular/common/http';
import {ServerService} from '../service/server.service';

@Component({
  selector: 'app-find-server',
  templateUrl: 'findServer.component.html',
  styleUrls: ['../modal/findServer.component.css']
})
export class FindServerComponent {
  public nameServer: string;

  constructor(public dialogRef: MatDialogRef<FindServerComponent>, private http: HttpClient,
              private serverService: ServerService, @Inject(MAT_DIALOG_DATA) public data: UserServer) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  findServer() {
    this.serverService.findServer(this.nameServer);
  }

}
