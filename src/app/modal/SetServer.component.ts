import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UserServer} from '../model/userServer';
import {TokenResponse} from '../model/TokenResponse';
import {HttpClient} from '@angular/common/http';
import {ServerService} from '../service/server.service';

@Component({
  selector: 'app-set-server',
  templateUrl: 'setServer.component.html',
  styleUrls: ['../modal/setServer.component.css']
})
export class SetServerComponent {
  public nameServer: string;
  private token: TokenResponse = JSON.parse(localStorage.getItem('authentication'));

  constructor(public dialogRef: MatDialogRef<SetServerComponent>, private http: HttpClient,
              private serverService: ServerService, @Inject(MAT_DIALOG_DATA) public data: UserServer) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  SetServer() {
    this.serverService.SetServer(this.nameServer);
  }

}
