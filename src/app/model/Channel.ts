export class Channel {
  id: bigint;
  serverid: bigint;
  channelid: bigint;
  namechannel: string;
}
