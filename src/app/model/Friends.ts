export class Friends {
  id: bigint;
  userid: bigint;
  friendid: number;
  groupid: bigint;
  token: string;
}
