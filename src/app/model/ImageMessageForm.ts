import {SafeUrl} from '@angular/platform-browser';

export class ImageMessageForm {
  image: File;
  channelId: number;
  url: SafeUrl;
}
