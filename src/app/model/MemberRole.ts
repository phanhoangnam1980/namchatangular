export class MemberRole {
  id: bigint;
  userid: bigint;
  rolecustom: bigint;
  serverid: bigint;
  sortnumber: bigint;
}
