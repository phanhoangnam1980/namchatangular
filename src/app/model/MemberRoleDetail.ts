export class MemberRoleDetail {
  id: bigint;
  role: string;
  serverid: bigint;
}
