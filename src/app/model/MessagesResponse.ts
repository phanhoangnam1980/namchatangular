import {User} from './User';
import {Friends} from './Friends';
import {Messages} from './messages';

export interface MessagesResponse {
  messagesresult: Array<Messages>;
  result: Array<User>;
  friendresult: Array<Friends>;
}
