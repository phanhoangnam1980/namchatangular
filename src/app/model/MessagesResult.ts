import {Messages} from './messages';
import {User} from './User';
import {Friends} from './Friends';

export interface MessagesResult {
  messagesresult: Messages;
  result: User;
  friendresult: Friends;
}
