export class Server {
  id: bigint;
  serverid: bigint;
  serverimage: string;
  nameserver: string;
  rulechannel: bigint;
}
