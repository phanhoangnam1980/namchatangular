import {UserServer} from './userServer';
import {Server} from './Server';
import {Channel} from './Channel';
import {MemberRole} from './MemberRole';
import {MemberRoleDetail} from './MemberRoleDetail';

export class ServerResult {
  member: number;
  userserver: Array<UserServer>;
  servers: Array<Server>;
  channel: Array<Channel>;
  userrole: Array<MemberRole>;
  roleserver: Array<MemberRoleDetail>;
}
