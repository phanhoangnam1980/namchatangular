export class TokenResponse {
  public tokenType: string;
  public accessToken: string;
}
