export class User {
  id: bigint;
  email: string;
  password: string;
  username: string;
  image: string;
  passwordConfirm: string;
}
