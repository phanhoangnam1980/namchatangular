export class Messages {
  id: number;
  userchats: string;
  groupid: number;
  session: string;
  sessionimage: string;
  datetime: string;
  Sortnumber: bigint;
  token: string;
}
