export interface UserServer {
  id: bigint;
  serverid: bigint;
  serverimage: string;
  nameserver: string;
}
