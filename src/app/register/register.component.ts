import {Component, Input, OnInit} from '@angular/core';
import {User} from '../model/User';
import {RegisterService} from '../service/register.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  @Input()
  user: User;
  error: string;

  constructor(private registerService: RegisterService, private router: Router) {
    this.user = new User();
  }

  ngOnInit(): void {
    this.registerService.getRegistration();
  }

  register() {
    this.registerService.postRegistration(this.user).subscribe(isValid => {
      if (isValid) {
        sessionStorage.setItem(
          'user',
          JSON.stringify(this.user.username)
        );
        localStorage.setItem('token', isValid.valid);
        this.router.navigate(['homepage']).then(
        );
      } else {
        this.error = 'register error';
      }
    });
  }
}
