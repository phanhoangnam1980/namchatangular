import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {MessagesResult} from '../model/MessagesResult';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {TokenResponse} from '../model/TokenResponse';

@Injectable({providedIn: 'root'})
export class FriendService {
  private token: TokenResponse = JSON.parse(localStorage.getItem('authentication'));
  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
    authorization: 'Bearer ' + this.token.accessToken
  });
  private serverPort = 'http://localhost:3000';

  constructor(private http: HttpClient) {
  }

  getFriendId(getId: number): Observable<object> {
    return this.http.post<MessagesResult>('http://localhost:8080/getid', JSON.stringify({getID: getId}),
      {headers: this.headers});
  }

  listFriend(): Observable<object> {
    return this.http.get<MessagesResult>(this.serverPort + '/listFriend',
      {headers: new HttpHeaders().set('authorization', 'Bearer ' + this.token.accessToken)});
  }
}
