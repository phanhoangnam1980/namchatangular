import {Injectable, OnDestroy} from '@angular/core';
import {MessagesResult} from '../model/MessagesResult';
import {BehaviorSubject} from 'rxjs';

@Injectable()

export class SwitchDataHomeService implements OnDestroy {

  private checkRequestFriend: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  currentCheckFriend = this.checkRequestFriend.asObservable();
  private FriendInfo: BehaviorSubject<MessagesResult> = new BehaviorSubject<MessagesResult>(null);
  currentFriendInfo = this.FriendInfo.asObservable();

  constructor() {
  }

  ngOnDestroy(): void {
    this.checkRequestFriend.unsubscribe();
    this.FriendInfo.unsubscribe();
  }

  setFriendInfo(friendInfo: MessagesResult) {
    this.FriendInfo.next(friendInfo);
  }

  setCheckRequest(CheckRequestFriend: boolean) {
    this.checkRequestFriend.next(CheckRequestFriend);
  }
}
