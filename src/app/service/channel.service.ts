import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {TokenResponse} from '../model/TokenResponse';
import {OnInit} from '@angular/core';
import {ServerResult} from '../model/ServerResult';

export class ChannelService implements OnInit {
  private token: TokenResponse = JSON.parse(localStorage.getItem('authentication'));
  private serverPort = 'http://localhost:3000';
  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
    authorization: 'Bearer ' + this.token.accessToken
  });

  ngOnInit(): void {
  }

  constructor(private http: HttpClient) {
  }

  creatNewChannel(nameChannel: string, serverId: number): Observable<object> {
    return this.http.post<ServerResult>(this.serverPort + '/creatnewchannel', JSON.stringify({
      namechannel: nameChannel,
      serverid: serverId
    }), {headers: this.headers});
  }

  listChannel(serverId: number): Observable<object> {
    return this.http.post<ServerResult>(this.serverPort + '/listchannel',
      JSON.stringify({serverid: serverId}), {headers: this.headers});
  }

  mappingChanel(serverId: number): Observable<object> {
    return this.http.post<ServerResult>(this.serverPort + '/mappingChannel', {serverid: serverId},
      {headers: this.headers});
  }

  mappingControl(serverId: number): Observable<object> {
    return this.http.post<ServerResult>(this.serverPort + '/mappingControl', JSON.stringify({serverid: serverId}),
      {headers: this.headers});
  }
}
