import {Injectable} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {ActivatedRoute} from '@angular/router';
import {ImageMessageForm} from '../model/ImageMessageForm';

@Injectable({providedIn: 'root'})
export class ImageService {

  constructor(private sanitizer: DomSanitizer) {
  }

  ClickFile(file, channelIdNumber) {
    const url = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(file));
    const newImageFile = new ImageMessageForm();
    newImageFile.channelId = channelIdNumber;
    newImageFile.image = file;
    newImageFile.url = url;
    return newImageFile;
  }
}
