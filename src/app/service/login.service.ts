import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {User} from '../model/User';
import {Observable} from 'rxjs';

@Injectable({providedIn: 'root'})
export class LoginService {

  private headers = new HttpHeaders({'Content-Type': 'application/json'});
  private serverPort = 'http://localhost:3000';

  constructor(private http: HttpClient) {
  }

  login(user: User): Observable<any> {
    return this.http.post(this.serverPort + '/signin', user, {headers: this.headers});
  }

}
