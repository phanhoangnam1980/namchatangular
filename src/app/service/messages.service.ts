import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {MessagesResult} from '../model/MessagesResult';
import {TokenResponse} from '../model/TokenResponse';
import {ImageMessageForm} from '../model/ImageMessageForm';
import {Messages} from '../model/messages';

@Injectable({providedIn: 'root'})
export class MessagesService {
  private token: TokenResponse = JSON.parse(localStorage.getItem('authentication'));
  private serverPort = 'http://localhost:3000';
  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
    authorization: 'Bearer ' + this.token.accessToken
  });

  constructor(private http: HttpClient) {
  }

  listMessages(group: number): Observable<object> {
    return this.http.post<MessagesResult>(this.serverPort + '/listMessages', JSON.stringify({groups: group}),
      {headers: this.headers});
  }

  nextMessages(group: number): Observable<object> {
    return this.http.post<MessagesResult>(this.serverPort + '/listnextMessages', JSON.stringify({groups: group}),
      {headers: this.headers});
  }

  getDetailId(id: number) {
    return this.http.post<MessagesResult>(this.serverPort + '/FriendDetail', {friendId: id},
      {headers: this.headers});
  }

  setUpDMGroup(id: number) {
    return this.http.post('http://localhost:8080/setgroups', JSON.stringify({friendId: id}),
      {headers: this.headers});
  }

  sendImageMessages(imageData: ImageMessageForm) {
    return this.http.post<Messages>('http://localhost:8080/sendImageMessages', imageData, {headers: this.headers});
  }
}
