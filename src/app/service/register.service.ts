import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../model/User';

@Injectable({providedIn: 'root'})
export class RegisterService {

  private headers = new HttpHeaders({'Content-Type': 'application/json'});
  private serverPort = 'http://localhost:3000';

  constructor(private http: HttpClient) {
  }

  getRegistration(): Observable<any> {
    return this.http.get(this.serverPort + '/registration', {headers: this.headers});
  }

  postRegistration(user: User): Observable<any> {
    return this.http.post(this.serverPort + '/signup', user, {headers: this.headers});
  }

}
