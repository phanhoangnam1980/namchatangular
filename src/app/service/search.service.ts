import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {TokenResponse} from '../model/TokenResponse';
import {MessagesResponse} from '../model/MessagesResponse';
import {Injectable} from '@angular/core';

@Injectable({providedIn: 'root'})
export class SearchService {
  private token: TokenResponse = JSON.parse(localStorage.getItem('authentication'));
  private serverPort = 'http://localhost:3000';

  constructor(private http: HttpClient) {
  }

  SearchResult(emails: string): Observable<object> {
    return this.http.post<MessagesResponse>(this.serverPort + '/searchUser', emails,
      {headers: new HttpHeaders().set('authorization', 'Bearer ' + this.token.accessToken)});
  }
}
