import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {OnInit} from '@angular/core';
import {TokenResponse} from '../model/TokenResponse';
import {ServerResult} from '../model/ServerResult';
import {UserServer} from '../model/userServer';

export class ServerService implements OnInit {
  private token: TokenResponse = JSON.parse(localStorage.getItem('authentication'));
  private serverPort = 'http://localhost:3000';

  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
  }

  listServer(): Observable<object> {
    return this.http.post<ServerResult>(this.serverPort + '/listserver', ' ',
      {headers: new HttpHeaders().set('authorization', 'Bearer ' + this.token.accessToken)});
  }

  SetServer(nameServer: string): Observable<object> {
    return this.http.post<ServerResult>(this.serverPort + '/creatnewserver', JSON.stringify({nameserver: nameServer}),
      {headers: new HttpHeaders().set('authorization', 'Bearer ' + this.token.accessToken)});
  }

  findServer(nameServer): Observable<object> {
    return this.http.post<UserServer>(this.serverPort + '/findserver', JSON.stringify({nameserver: nameServer}),
      {headers: new HttpHeaders().set('authorization', 'Bearer ' + this.token.accessToken)});
  }
}
