import io from 'socket.io-client';
import {Messages} from '../model/messages';
import {environment} from '../../environments/environment';

export class SocketService {
  private socketIO;
  public MessageNext: Array<Messages> = [];

  constructor() {
  }

  startChat() {
    this.socketIO = io(environment.SOCKET_ENDPOINT);
  }

  sendMessage(messagesData: Messages) {
    this.socketIO.emit('sendMessage', JSON.stringify(messagesData));
  }

  sendTyping(nameUser: string, flag: boolean) {
    if (flag === true) {
      this.socketIO.emit('typing', JSON.stringify({User: nameUser}));
    } else {
      this.socketIO.emit('timeoutTyping', JSON.stringify({User: nameUser}));
    }
  }
}
